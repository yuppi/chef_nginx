#
# Cookbook Name:: nginx
# Recipe:: default
#
# Copyright 2014, Suzuki-Yuki
#
# All rights reserved - Do Not Redistribute
#
#--------------------------------------------------------------------
# nginx公式リポジトリを取得する
#--------------------------------------------------------------------
# nginx公式リポジトリを置くディレクトリを作成
directory node['nginx']['default']['work_dir'] do
    action :create
    recursive true
    not_if "ls -d #{node['nginx']['default']['work_dir']}"
end


# nginx公式リポジトリ情報をDL＠RHEL6
remote_file node['nginx']['default']['work_dir'] + node['nginx']['default']['source_file_name'] do
    source node['nginx']['default']['repository_url'] + node['nginx']['default']['source_file_name']
    not_if "ls #{node['nginx']['default']['work_dir']}#{node['nginx']['default']['source_file_name']}"
end

#--------------------------------------------------------------------
# nginx公式のリポジトリを追加する
#--------------------------------------------------------------------
# RPMをインストール
rpm_package "nginx-repos-rpm" do
    action :install
    source node['nginx']['default']['work_dir'] + node['nginx']['default']['source_file_name']
end

#--------------------------------------------------------------------
# nginxのインストール
#--------------------------------------------------------------------
package "nginx" do
    action :install
    options "--enablerepo=epel --enablerepo=remi"
end

#--------------------------------------------------------------------
# nginx.confの設定
#--------------------------------------------------------------------
template "nginx.conf" do
    path "/etc/nginx/nginx.conf"
    source "nginx.conf.erb"
    mode 0644
end

#--------------------------------------------------------------------
#  default.confの削除
#--------------------------------------------------------------------
file "default.conf" do
    path "/etc/nginx/conf.d/default.conf"
    action :delete
    only_if { ::File.exists?("/etc/nginx/conf.d/default.conf") }
end

#--------------------------------------------------------------------
#  example_ssl.confの削除
#--------------------------------------------------------------------
file "example_ssl.conf" do
    path "/etc/nginx/conf.d/example_ssl.conf"
    action :delete
    only_if { ::File.exists?("/etc/nginx/conf.d/example_ssl.conf") }
end

#--------------------------------------------------------------------
# nginxサービスの自動起動設定
#--------------------------------------------------------------------
service "nginx" do
    action [:start, :enable]
    supports :status => true, :restart => true, :reload => true
end