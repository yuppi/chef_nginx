#
# Cookbook Name:: nginx
# Recipe:: toslove
#
# Copyright 2014, Suzuki-Yuki
#
# All rights reserved - Do Not Redistribute
#
#--------------------------------------------------------------------
# toslove.confの設定
#--------------------------------------------------------------------
template "toslove.conf" do
    path "/etc/nginx/conf.d/toslove.conf"
    source "toslove.conf.erb"
    mode 0644
    notifies :restart, 'service[nginx]', :immediately
end