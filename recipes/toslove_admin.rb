#
# Cookbook Name:: nginx
# Recipe:: toslove_admin
#
# Copyright 2014, Suzuki-Yuki
#
# All rights reserved - Do Not Redistribute
#
#--------------------------------------------------------------------
# toslove_admin.confの設定
#--------------------------------------------------------------------
template "toslove_admin.conf" do
    path "/etc/nginx/conf.d/toslove_admin.conf"
    source "toslove_admin.conf.erb"
    mode 0644
    notifies :restart, 'service[nginx]', :immediately
end