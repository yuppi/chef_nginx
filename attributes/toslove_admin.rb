#--------------------------------------------------------------------
# toslove_admin.confの設定
#--------------------------------------------------------------------
# listenポート番号
default['nginx']['toslove_admin']['toslove_admin.conf']['listen'] = 80
# サーバー名を設定します。
default['nginx']['toslove_admin']['toslove_admin.conf']['server_name'] = "localhost"
# ドキュメントルート
default['nginx']['toslove_admin']['toslove_admin.conf']['root'] = "/vagrant/public"
# アクセスログの場所
default['nginx']['toslove_admin']['toslove_admin.conf']['access_log'] = "/var/log/nginx/laravel_access.log"
# エラーログの場所
default['nginx']['toslove_admin']['toslove_admin.conf']['error_log'] = "/var/log/nginx/laravel_error.log"