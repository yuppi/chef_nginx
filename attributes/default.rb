# 作業用ディレクトリ
default['nginx']['default']['work_dir']  = '/usr/local/src/nginx/'

# ソースコードの URL
default['nginx']['default']['repository_url']      = "http://nginx.org/packages/centos/6/noarch/RPMS/"
default['nginx']['default']['source_file_name']    = "nginx-release-centos-6-0.el6.ngx.noarch.rpm"

#--------------------------------------------------------------------
# nginx.confの設定
#--------------------------------------------------------------------
# nginxのworkerプロセスの数を設定します。通常はCPUのコア数以下に設定します。
default['nginx']['default']['nginx.conf']['worker_procesces'] = 4
# 一つのworkerプロセスが同時に処理できる最大コネクション数を設定します。
default['nginx']['default']['nginx.conf']['worker_connections'] = 1024
# アクセスログの場所と種類を設定します。
default['nginx']['default']['nginx.conf']['access_log'] = "/var/log/nginx/access.log  main"
# エラーログの場所と種類を設定します。
default['nginx']['default']['nginx.conf']['error_log'] = "/var/log/nginx/error.log warn"
# 文字セット
default['nginx']['default']['nginx.conf']['charset'] = "UTF-8"
# vagrantの場合はここをOFFにしないと更新ファイルが反映されない場合がある
# 参考：http://qiita.com/shoyan/items/12389d5beaa8695b1a53
default['nginx']['default']['nginx.conf']['sendfile'] = "off"
# keepalive time
default['nginx']['default']['nginx.conf']['keepalive_timeout'] = 65

