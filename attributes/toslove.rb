#--------------------------------------------------------------------
# toslove.confの設定
#--------------------------------------------------------------------
# listenポート番号
default['nginx']['toslove']['toslove.conf']['listen'] = 80
# サーバー名を設定します。
default['nginx']['toslove']['toslove.conf']['server_name'] = "localhost"
# ドキュメントルート
default['nginx']['toslove']['toslove.conf']['root'] = "/vagrant/public"
# アクセスログの場所
default['nginx']['toslove']['toslove.conf']['access_log'] = "/var/log/nginx/laravel_access.log"
# エラーログの場所
default['nginx']['toslove']['toslove.conf']['error_log'] = "/var/log/nginx/laravel_error.log"